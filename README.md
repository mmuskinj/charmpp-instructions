# Input files 

Output histograms from charmpp are located here: `/global/cfs/cdirs/atlas/wcharm/for_alec/charmpp-output-v13-1`. There are multiple files, each corresponding to a different MC sample:

```

data.root

Diboson.root

MG_NLO_WplusD.root

MG_Wjets_bjets_emu.root

MG_Wjets_cjets_emu.root

MG_Wjets_emu.root

MG_Wjets_light_emu.root

MG_Wjets_tau.root

Sh_2211_Wjets_bjets_emu.root

Sh_2211_Wjets_cjets_emu.root

Sh_2211_Wjets_light_emu.root

Sh_2211_Wjets_tau.root

Sh_2211_WplusD.root

Sh_2211_Zjets_bjets_emu.root

Sh_2211_Zjets_cjets_emu.root

Sh_2211_Zjets_light_emu.root

Sh_2211_Zjets_tau.root

Sherpa2211_Zjets.root

Top_single_top.root

Top_ttbar_Shower.root

Top_ttbar.root

Top_ttx.root

```

  

- `data` is the data,

- `MG_Wjets` are MadGraph+Pythia8 W+jets samples in various compositions

- `Sh_2211_Wjets` same except Sherpa2.2.11

- `Sh2211_Zjets` same for the Z+jets process

- `MG_NLO_WplusD` and `Sh_2211_WplusD` are the special "signal" samples that we use for the W+D process

- `Top_ttbar` Powheg+Pythia8 ttbar sample

- `Top_ttbar_Shower` Powheg+Herwig7 ttbar sample

- `Top_single_top` and `Top_ttx` are backgrounds involving top quark processes

- `Diboson` backgrounds from multi-boson production

  

Inspect any of the files with root from the command line to see the available histograms:

  

```

> rootls Sh_2211_WplusD.root | grep mu_minus_SR_0tag_Dplus_OS_Matched__

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_chi2

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_cosThetaStar

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_cTau

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_d0

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_dRlep

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_DsMin

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_DstarMin

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_eta

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_impact_sig

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_kaonPassTight

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_Lxy

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_m

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_max_trackdR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_max_trackZ0

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_max_trackZ0PV

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_min_trackPt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_m

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_10_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_10_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_10_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_6_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_6_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_6_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_8_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_8_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_peak_track_jet_8_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_phi

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_ptcone40_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_SVr

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_10_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_10_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_10_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_6_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_6_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_6_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_8_dR

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_8_pt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_track_jet_8_zt

mu_minus_SR_0tag_Dplus_OS_Matched__Dmeson_z0sinTheta

mu_minus_SR_0tag_Dplus_OS_Matched__lep_charge

mu_minus_SR_0tag_Dplus_OS_Matched__lep_d0

mu_minus_SR_0tag_Dplus_OS_Matched__lep_d0_fixed

mu_minus_SR_0tag_Dplus_OS_Matched__lep_d0sig

mu_minus_SR_0tag_Dplus_OS_Matched__lep_d0sig_fixed

mu_minus_SR_0tag_Dplus_OS_Matched__lep_dR_Dmeson

mu_minus_SR_0tag_Dplus_OS_Matched__lep_eta

mu_minus_SR_0tag_Dplus_OS_Matched__lep_is_isolated

mu_minus_SR_0tag_Dplus_OS_Matched__lep_newflowisol_over_pt

mu_minus_SR_0tag_Dplus_OS_Matched__lep_phi

mu_minus_SR_0tag_Dplus_OS_Matched__lep_pt

mu_minus_SR_0tag_Dplus_OS_Matched__lep_ptvarcone20_TightTTVA_pt1000

mu_minus_SR_0tag_Dplus_OS_Matched__lep_ptvarcone20_TightTTVA_pt1000_over_pt

mu_minus_SR_0tag_Dplus_OS_Matched__lep_ptvarcone30_TightTTVA_pt1000

mu_minus_SR_0tag_Dplus_OS_Matched__lep_ptvarcone30_TightTTVA_pt1000_over_pt

mu_minus_SR_0tag_Dplus_OS_Matched__lep_topoetcone20

mu_minus_SR_0tag_Dplus_OS_Matched__lep_topoetcone20_over_pt

mu_minus_SR_0tag_Dplus_OS_Matched__lep_z0sinTheta

mu_minus_SR_0tag_Dplus_OS_Matched__met_dphi

mu_minus_SR_0tag_Dplus_OS_Matched__met_met

mu_minus_SR_0tag_Dplus_OS_Matched__met_mt

mu_minus_SR_0tag_Dplus_OS_Matched__N_Dplus

mu_minus_SR_0tag_Dplus_OS_Matched__nbjets

mu_minus_SR_0tag_Dplus_OS_Matched__njets

mu_minus_SR_0tag_Dplus_OS_Matched__pileup

mu_minus_SR_0tag_Dplus_OS_Matched__run_number

mu_minus_SR_0tag_Dplus_OS_Matched__truth_Dmeson_eta

mu_minus_SR_0tag_Dplus_OS_Matched__truth_Dmeson_lep_eta

mu_minus_SR_0tag_Dplus_OS_Matched__truth_Dmeson_mass

mu_minus_SR_0tag_Dplus_OS_Matched__truth_Dmeson_pt

mu_minus_SR_0tag_Dplus_OS_Matched__truth_lep_eta

mu_minus_SR_0tag_Dplus_OS_Matched__truth_lep_phi

mu_minus_SR_0tag_Dplus_OS_Matched__truth_lep_pt

mu_minus_SR_0tag_Dplus_OS_Matched__truth_met_met

mu_minus_SR_0tag_Dplus_OS_Matched__truth_met_mt

mu_minus_SR_0tag_Dplus_OS_Matched__truth_v_eta

mu_minus_SR_0tag_Dplus_OS_Matched__truth_v_m

mu_minus_SR_0tag_Dplus_OS_Matched__truth_v_phi

mu_minus_SR_0tag_Dplus_OS_Matched__truth_v_pt

```

  

With `grep` I singled out a single channel because the histograms are the same for all channels.

  

# Making data / MC plots

  

The data / MC plots are made with the charmplot python + ROOT library: https://gitlab.cern.ch/berkeleylab/CharmPhysics/charmplot/. I also put a copy of the library in a public folder on cori: /global/cfs/cdirs/atlas/wcharm/for_alec/charmplot. To run the plotting code you need an environment with python 3 and ROOT. I have a conda environment that I use for this purpose. Cesar should also be able to help setting this up.

  

Once the environment is setup, you can copy the `charmpp` outputs to a new folder and run the plotting code from there. For example:

```

cd $CSCRATCH

mkdir test-plot

cd test-plot

cp -r

cp /global/cfs/cdirs/atlas/wcharm/for_alec/charmpp-output-v13-1/* .

```

  

This creates the new folder and copies the charmpp outputs to that folder. Now run the plotting code:

```

plot_data_mc.py -a charm_frag/wplusd_sherpa -v Dmeson_m_fit,Dmeson_peak_m_fit,Dmeson_peak_pt,Dmeson_peak_track_jet_10_pt,Dmeson_peak_track_jet_10_dR,Dmeson_peak_track_jet_10_zt

```

  

The `plot_data_mc.py` file is a script from the `charmplot` library. The `-a` option specifies the analysis. The `-v` option specifies the variables to plot. The variables are separated by commas. The variables are the names of the histograms in the charmpp output. The `plot_data_mc.py` script will automatically find the histograms in the charmpp output and make the data / MC plots.

  

Multiple "analyses" are available and they are configured via .yaml files in the `charmplot` library:

```

> ls -l charmplot/config/charm_frag

ttbar_herwig.yaml

ttbar_pythia.yaml

wplusd_madgraph.yaml

wplusd_sherpa.yaml

```

  

The `ttbar` files plot the "1-tag"  and  "2-tag" regions corresponding to the ttbar with W->cs events. We can use either the Powheg+Pythia or Powheg+Herwig samples. The `wplusd` files are the 0-tag W+D region. Here we have MG+Pythia8 and Sherpa samples. Similarly, the variables specified by `-v` are configures in the `charmplot/config/variables/charmed_wjets.yaml` file, e.g.:

  

```

> cat charmplot/config/variables/charmed_wjets.yaml | grep Dmeson_peak_pt -A 10

Dmeson_peak_pt:

label: 'D^{#pm} p_{T} (peak region)'

unit: 'GeV'

rebin: 5

ratio_range: [0.5, 1.49]

```

  

The output from the above `plot_data_mc.py` command is a bunch of pdf files in the current directory. The pdf files are named according to the analysis and variable, e.g.:

```

> tree charm_frag

charm_frag

`-- wplusd_sherpa

|-- OS-SS_0tag_Dplus

| |-- OS-SS_0tag_Dplus_Dmeson_m_fit.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_m_fit_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_m_fit.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_m_fit_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_pt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_pt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_dR.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_dR_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_pt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_pt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_zt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_10_zt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_dR.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_dR_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_pt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_pt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_zt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_6_zt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_dR.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_dR_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_pt.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_pt_LOG.pdf

| |-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_zt.pdf

| `-- OS-SS_0tag_Dplus_Dmeson_peak_track_jet_8_zt_LOG.pdf

|-- OS-SS_0tag_Dplus.pdf

`-- histograms.root

```
